#!/usr/bin/env python
import riders_turtlebot
import rospy
import time 

#setup
rospy.init_node("turtlebot")
turtlebot = riders_turtlebot.RidersTurtlebot("robot")
time.sleep(2)

#loop
while True:
    try:
        front_color_sensor = turtlebot.read_front_color_sensor()
        back_color = turtlebot.read_back_color_sensor().get_brightness()
        right_color = turtlebot.read_right_color_sensor().get_brightness()
        left_color = turtlebot.read_left_color_sensor().get_brightness()
        
        middle_distance = turtlebot.read_middle_distance_sensor()
        right_distance = turtlebot.read_right_distance_sensor()
        left_distance = turtlebot.read_left_distance_sensor()

        error = (left_distance - right_distance)
        turtlebot.move(0.5)
        turtlebot.rotate(error)

        pose = turtlebot.get_odom()


    except (KeyboardInterrupt, SystemExit):
        print("break")
        break
        