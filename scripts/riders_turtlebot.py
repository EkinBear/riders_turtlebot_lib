#!/usr/bin/env python

import rospy
import math
import sys
import json

from std_msgs.msg import String
from geometry_msgs.msg import Twist
from gazebo_msgs.srv import GetModelState
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import Range
from sensor_msgs.msg import Image

from riders_turtlebot_lib.srv import ColorSensor
from riders_turtlebot_lib.srv import LaserSensor

class Color:
    def __init__(self, r = 0, g = 0, b = 0, brightness = 0):
        self.r = r
        self.b = b
        self.g = g

    def get_brightness(self):
        y = 0.2126 * self.r + 0.7152 * self.g + 0.0722 * self.b
        return y

    def string_to_rgb(self, data):
        self.r = ord(data[0])
        self.g = ord(data[1])
        self.b = ord(data[2])

class ColorSensorRow:

    def __init__(self):
        self.height = 0
        self.width = 0
        self.size = 0
        self.color_row = []

    def parse_data(self, msg):
        self.color_row = []

        if not (self.height == msg.height and self.width == msg.width):
            self.height = msg.height
            self.width = msg.width
            self.size = self.height * self.width

        for i in range(self.size):
            pixel = Color()
            pixel.string_to_rgb([msg.data[i*3], msg.data[i*3 + 1], msg.data[i*3 + 2]])
            self.color_row.append(pixel)

class Vector3:
    def __init__(self, x = 0, y = 0, z = 0):
        self.x = x
        self.y = y
        self.z = z

class Quaternion:
    def __init__(self, x = 0, y = 0, z = 0, w = 0):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

class EulerAngles:

    def __init__(self, roll = 0, pitch = 0, yaw = 0):
        self.roll = roll
        self.pitch = pitch
        self.yaw = yaw

    def quaternion_to_euler_angles(self, q):
        # https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        # roll (x-axis rotation)
        sinr_cosp = 2 * (q.w * q.x + q.y * q.z)
        cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y)
        self.roll = math.atan2(sinr_cosp, cosr_cosp)

        # pitch (y-axis rotation)
        sinp = 2 * (q.w * q.y - q.z * q.x)
        if abs(sinp) >= 1:
            self.pitch = math.copysign(math.pi/2, sinp)
        else:
            self.pitch = math.asin(sinp)

        # yaw (z-axis rotation)
        siny_cosp = 2 * (q.w * q.z + q.x * q.y)
        cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z)
        self.yaw = math.atan2(siny_cosp, cosy_cosp)

class Pose:
    position = Vector3()
    orientation = EulerAngles()


class RidersTurtlebot:
    def __init__(self, robot_name):

        self.__metrics = {
            "sim_time": "0",
            "velocity": "0",
            "robot_name": robot_name,
            "middle_distance": "0",
            "left_distance": "0",
            "right_distance": "0"
        }
        
    
        self.robot_name = robot_name

        self.__temp_angle = 0
        self.__i_angle = 0

        self.__angle_kp = 2
        self.__angle_ki = 0
        self.__angle_kd = 0

        self.msg = Twist()

        self.__middle_range = 0
        self.__front_left_range = 0
        self.__front_right_range = 0

        self.__front_color = ColorSensorRow()
        self.__left_color = Color()
        self.__right_color = Color()
        self.__back_color = Color()

        self.__pub = rospy.Publisher(("/%s/cmd_vel/" % self.robot_name), Twist, queue_size=10)
        self.__pub.publish(self.msg)

        self.__metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)

    def update(self):
        self.__pub.publish(self.msg)

    def move(self, velocity):
        self.msg.linear.x = velocity
        self.update()

    def stop(self):
        null_msg = Twist()
        self.msg = null_msg
        self.update()

    def rotate(self, rotation):
        self.msg.angular.z = rotation
        self.update()

    def read_left_distance_sensor(self):
        try:
            read_left_distance_service = rospy.ServiceProxy(("/%s/front_left_sonar_sensor_frame/get_range" % self.robot_name), LaserSensor)
            response = read_left_distance_service()
            return response.data
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def read_right_distance_sensor(self):
        try:
            read_right_distance_service = rospy.ServiceProxy(("/%s/front_right_sonar_sensor_frame/get_range" % self.robot_name), LaserSensor)
            response = read_right_distance_service()
            return response.data
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def read_middle_distance_sensor(self):
        try:
            read_middle_distance_service = rospy.ServiceProxy(("/%s/middle_sonar_sensor_frame/get_range" % self.robot_name), LaserSensor)
            response = read_middle_distance_service()
            return response.data
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e


    def read_front_color_sensor(self):
        try:
            read_front_camera_service = rospy.ServiceProxy(("/%s/front_color_sensor_frame/get_image" % self.robot_name), ColorSensor)
            response = read_front_camera_service()
            self.__front_color.parse_data(response)
            return self.__front_color.color_row

        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def read_left_color_sensor(self):
        try:
            read_front_camera_service = rospy.ServiceProxy(("/%s/left_color_sensor_frame/get_image" % self.robot_name), ColorSensor)
            response = read_front_camera_service()
            self.__left_color.string_to_rgb(response.data)
            return self.__left_color
            
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def read_right_color_sensor(self):
        try:
            read_front_camera_service = rospy.ServiceProxy(("/%s/right_color_sensor_frame/get_image" % self.robot_name), ColorSensor)
            response = read_front_camera_service()
            self.__right_color.string_to_rgb(response.data)
            return self.__right_color
            
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def read_back_color_sensor(self):
        try:
            read_front_camera_service = rospy.ServiceProxy(("/%s/back_color_sensor_frame/get_image" % self.robot_name), ColorSensor)
            response = read_front_camera_service()
            self.__back_color.string_to_rgb(response.data)
            return self.__back_color
            
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def get_odom(self):
        model_state = self.__call_get_model_state(self.robot_name)
        q = Quaternion(model_state.pose.orientation.x, model_state.pose.orientation.y, model_state.pose.orientation.z, model_state.pose.orientation.w)
        
        pose = Pose()
        pose.position.x = model_state.pose.position.x
        pose.position.y = model_state.pose.position.y
        pose.position.z = model_state.pose.position.z
        pose.orientation.quaternion_to_euler_angles(q)
        return pose

    def __call_get_model_state(self, model_name):
        rospy.wait_for_service('/gazebo/get_model_state')
        try:
            get_model_state = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
            response = get_model_state(model_name, None)
            return response
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def __get_sim_time(self):
        rospy.wait_for_service('/gazebo/get_world_properties')
        try:
            world = rospy.ServiceProxy('/gazebo/get_world_properties', GetWorldProperties)
            response = world()
            return response.sim_time
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def __get_model_vel(self, model_name):
        try:
            response = self.__call_get_model_state(model_name)
            vel_x = response.twist.linear.x
            vel_y = response.twist.linear.y
            vel_z = response.twist.linear.z
            velocity = math.sqrt(vel_x**2 + vel_y**2 + vel_z**2)
            
            return velocity
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def __publish_metrics(self):
        self.__metrics["sim_time"] = self.__get_sim_time()
        self.__metrics["velocity"] = self.__get_model_vel(self.robot_name)
        self.__metrics["left_distance"] = str(self.read_left_distance_sensor())
        self.__metrics["right_distance"] = str(self.read_right_distance_sensor())
        self.__metrics["middle_distance"] = str(self.read_middle_distance_sensor())

        self.__metric_pub.publish(json.dumps(self.__metrics, sort_keys=True))
